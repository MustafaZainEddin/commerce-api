class CreatePromotions < ActiveRecord::Migration[5.2]
  def change
    create_table :promotions do |t|
      t.string :code
      t.string :category
      t.float :value
      t.float :max_value

      t.timestamps
    end
  end
end
