class CreateProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :products do |t|
      t.string :name
      t.float :price
      t.string :material
      t.string :color
      t.references :department, foreign_key: true
      t.references :promotion, foreign_key: true

      t.timestamps
    end
  end
end
