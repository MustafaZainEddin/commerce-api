# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

departments = []

10.times do
  departments << Department.create(name: Faker::Commerce.department(1, true))
end

promotions = []
promo_categories = Promotion.enumerized_attributes.attributes["category"].values
20.times do
  promo_category = promo_categories.sample
  max_value = rand(10..200)  # Assume min discount 10 $ and max discount 200 $

  if promo_category == 'percentage'
    value = (rand * 30).round(2) # Assume max discount 30%
  else
    value = max_value
  end

  promotions << Promotion.create(
      code: Faker::Commerce.promotion_code(digits = 2),
      category: promo_category,
      value: value,
      max_value: max_value
  )
end

100.times do
  min_price = 50 # Assume min product price is 50$
  promotion = nil

  has_promo = Faker::Boolean.boolean(0.7)
  if has_promo
    promotion = promotions.sample
    min_price = promotion.max_value
  end

  Product.create(
             name: Faker::Commerce.product_name,
             price: Faker::Commerce.price(range = min_price..2000), # Assume max product price is 2k$
             material: Faker::Commerce.material,
             color: Faker::Commerce.color,
             department: departments.sample,
             promotion: promotion
  )
end