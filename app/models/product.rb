class Product < ApplicationRecord
  belongs_to :department
  belongs_to :promotion, optional: true

  scope :filter, -> (filter_params) {
    departments(filter_params[:department_id]).
        promotions(filter_params[:promotion_code]).
        name_filter(filter_params[:product_name])
  }
  scope :departments, -> (department_id) {where(department_id: department_id) if department_id.present?}
  scope :promotions, -> (promotion_code) {where(promotion_id: Promotion.find_by(code: promotion_code)) if promotion_code.present?}
  scope :name_filter, -> (product_name) {where("name LIKE ?", "%#{product_name}%") if product_name.present?}

  def discounted_price
    if promotion.present?
      promotion.calculate_price(price)
    else
      price
    end
  end
end
