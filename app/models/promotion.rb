class Promotion < ApplicationRecord
  extend Enumerize
  enumerize :category, in: [:fixed, :percentage]

  has_many :products

  def calculate_price(price)
    if category.percentage?
      discounted_value = [(price * value / 100), max_value].min
    else
      discounted_value = value
    end

    (price - discounted_value).round(2)
  end
end
