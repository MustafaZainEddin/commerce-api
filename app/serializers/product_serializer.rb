class ProductSerializer < ActiveModel::Serializer
  attributes :id, :name, :price, :material, :color
  attributes :department
  attributes :promotion
  attribute :discounted_price, key: :final
end
